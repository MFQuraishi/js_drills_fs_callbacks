const fs = require("fs");

function createAndDelete(contentArray, pathArray) {
  if (!Array.isArray(contentArray) || !Array.isArray(pathArray)) {
    throw new Error("invalid arguments passed");
  }

  contentArray.forEach((content, index) => {
    createFile(content, pathArray[index]);
  });

  setTimeout(() => {
    pathArray.forEach((path) => {
      deleteFile(path);
    });
  }, 6000);
}

function createFile(content, path) {
  if (typeof content !== "object" || typeof path !== "string") {
    throw new Error("invalid arguments passed");
  }

  fs.writeFile(path, JSON.stringify(content), (error) => {
    if (error) {
      console.error(error);
    }
  });
}

function deleteFile(path) {
  if (typeof path !== "string") {
    throw new Error("invalid arguments passed");
  }

  let delay = 0;
  let interval = setInterval(() => {
    fs.exists(path, (exist) => {
      if (exist) {
        fs.rm(path, (err) => {
          if (err) {
            console.error(err);
          }
        });
        clearInterval(interval);
      }
      else if(delay >= 10000){
          console.log(`cannot find file at ${path}: stopping to try to delete it`);
          clearInterval(interval)
      }
      else{
          delay+=100;
      }
    });
  }, 100);
}

module.exports = {
  createAndDelete,
};
