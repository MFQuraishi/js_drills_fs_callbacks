const fs = require("fs");

function doOperations(){

    let pathToLipsum = "./data/lipsum.txt";
    let outputPath = "./output/";

    let fileNameTxtFile = "./output/filenames.txt"

    // read lipsum.txt
    fs.readFile(pathToLipsum, "utf8", (err, data)=> {
        if (err){
            console.error(err);
        }
        let upperCase = toUpperCase(data);
        // writing upper case to uppercase.txt
        fs.writeFile(outputPath + "uppercase.txt", upperCase, (err)=>{
            if (err) {
                console.error(err);
            }
            // writing name of file uppercase.txt to filenames.txt
            fs.writeFile(fileNameTxtFile, "uppercase.txt", (err) => {
                if (err){
                    console.error(err);
                }
                //reading uppercase.txt
                fs.readFile(outputPath + "uppercase.txt", "utf8", (err, data) => {
                    if (err){
                        console.error(err);
                    }
                    // converting to lower case
                    let lowerCaseSplit = toLowerCase(data).split(".").join("\n");
                    // writing to lowercase.txt
                    fs.writeFile(outputPath + "lowercase.txt", lowerCaseSplit, (err) => {
                        if (err){
                            console.error(err);
                        }
                        // appending file name "lowercase.txt" to filenames.txt
                        fs.appendFile(fileNameTxtFile, "\nlowercase.txt", (err) => {
                            if (err){
                                console.error(err);
                            }
                            // reading content of lowercase.txt
                            fs.readFile(outputPath + "lowercase.txt", "utf8", (err, data)=>{
                                if (err) {
                                    console.error(error);
                                }

                                let fileData = data.split("\n");
                                let sorted = sortSentences(fileData).join("\n");
                                // write sorted sentences to sortedSentences.txt
                                fs.writeFile(outputPath + "sortedSentences.txt", sorted, (err) => {
                                    if (err){
                                        console.error(err);
                                    }
                                    // append sortedSentences.txt to filenames.txt
                                    fs.appendFile(fileNameTxtFile, "\nsortedSentences.txt", (err) => {
                                        if (err) {
                                            console.error(err);
                                        }
                                        // read content of filenames.txt
                                        fs.readFile(outputPath + "filenames.txt", "utf8", (err, data) => {
                                            if (err){
                                                console.error(err);
                                            }

                                            let files = data.split("\n");
                                            // wait for 4 secs and delete all files that were created
                                            setTimeout(() => {
                                                files.forEach((file) => {
                                                    fs.rm(outputPath + file, (err) => {
                                                        if (err) {
                                                            console.log(err);
                                                        }
                                                    });
                                                });
                                            },4000);
                                        })
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
}

function sortSentences(arr){
    if (!Array.isArray(arr)){
        throw new Error("invalid argument passed");
    }
    return arr.sort();
}


function toLowerCase(string){
    if (typeof string !== "string"){
        throw new Error("invalid argument passed");
    }
    return string.toLowerCase();
}

function toUpperCase(string){
    if (typeof string !== "string"){
        throw new Error("invalid argument passed");
    }
    return string.toUpperCase();
}

module.exports = {
    doOperations
}