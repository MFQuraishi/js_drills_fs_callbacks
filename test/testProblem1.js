const {createAndDelete} = require("./../problem1");

const contents = [
    {a: 1},
    {b: 2},
    {c: 3},
    {d: 4}
];

const paths = [
    "./output/1.json",
    "./output/2.json",
    "./output/3.json",
    "./output/4.json"
]

createAndDelete(contents, paths)